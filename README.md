# Hàësdáïga translation for XKCD comics

The original comics are licensed under CC-BY-ND 2.5, and can be found at
<https://xkcd.com>.

Each translation will be in three files:

- `<number>.png` the comic
- `<number>.txt` text used for generating the script
- `<number>.gloss` the gloss

where `<number>` is the number associated with the comic strip.  For example,
`2462.png` will be the translation for the comic <https://xkcd.com/2462/>.
The title text will be put in a parentheses below the comic translation.

<a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/4.0/">
  <img alt="Creative Commons License"
       style="border-width:0"
       src="https://i.creativecommons.org/l/by-nc-nd/4.0/88x31.png" />
</a>
<br />
This work is licensed under a 
<a rel="license"
   href="http://creativecommons.org/licenses/by-nc-nd/4.0/">
Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License</a>.

# Characters' grammatical gender

Real people's gender are determined by the month they were born in lunar year.
We however don't know such information about the characters in the comics, so
let's set up some convention here for consistency:

- Cueball: metal
- Megan: wood
- Ponytail: fire
- Hairy: wood
- Black Hat: water
- White Hat: earth

These choices are purely random.
